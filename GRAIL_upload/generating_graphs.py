# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

"""
When plotting the left side, you only want the ones with `L`. If you plot the ones that have `R`, that means you are
plotting the right leg but from left heel strike to left heel strike (and while it's not by definition "wrong",
we are so used to the convention of plotting the data from the heel strike of the same leg, i.e. left side joints
from left heel strike to left heel strike, that it looks strange).
There are some that you want to plot for both, these are the `PelvicObl`, `PelvicRot` and `PelvicTil` and all the
`Trunk` values too. This is just because we only have one trunk and pelvis not two!
For the Forces, `FP0` is left and `FP1` is right leg
The value of the tag `leftside` should be set to `1` for left cycles and `0` for right cycles
"""

import logging
import re
from math import isnan
from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import PySimpleGUI as sg

from GRAIL_upload.config import (EXCLUDE_GRAPHS, GRAIL_GRAPH_MEAN_COLOR, GRAIL_GRAPH_NORM_COLOR, GRAIL_GRAPH_STD_COLOR,
                                 GRAIL_GRAPH_X_COLOR, GRAPH_DIR, GRAPH_OUTPUT_FORMAT, GRAPHS_NOT_USED, LAYOUT_FILE_RUN,
                                 LAYOUT_FILE_WALK, NAME_LAYOUT_FILE_RUN, NAME_LAYOUT_FILE_WALK, NORM_VALUE, README_TEXT,
                                 STANDARD_VALUE)
from GRAIL_upload.read_layout import (create_dataframe, df_cols_to_json, enhance_layout_file, get_lookup_elements,
                                      get_root, save_json)
from GRAIL_upload.utils import get_csv_files

# DATA FROM LAYOUT FILE: ------------------------------------------------------------------------------


def remove_nan_values(types_dict: dict) -> dict:
    """ Function deletes key-value-pairs if value is NaN
    param types_dict: dictionary, dataframe from layout-file
    return: dictionary, dataframe from layout-file without NaN-values
    """

    for k, v in types_dict.copy().items():  # copies the keys - otherwise runtime error 'dictionary changed size
        # during iteration'
        if isinstance(v, float) and isnan(v):
            types_dict.pop(k)
    return types_dict


def get_layout_vars(dataframe_layout: pd.DataFrame, channelname: str, var: str) -> str:
    """
    Function with three parameters, retrieves values from layout-file.
    param dataframe_layout: pandas dataframe from layout file, imported from read_layout.py
    param channelname: string values from function get_all_vars_in_specific_column(dataframe, col), these are all the
    variables in the .csv-file used for measuring, e.g. "Rotation TrunkTilt" or "LProgression". The tag in the
    layout-file is "channelname"
    param var: string, column name from dataframe_layout, here: 'title', 'verticalaxislabel', 'normdatamin',
     'normdatamax'
    return: string values of dataframe_layout for channelname and var (e.g. "Knie Ab - Adduktion", "left",
    "⬅ ad   (Nm/kg) ab ➡" and two long lists with values)
    """

    raw_dict = dict(dataframe_layout[['channelname', var]].values)  # create dictionary, k: value of channelname-col,
    # v: value of var-col
    clean_dict = remove_nan_values(raw_dict)

    if channelname in clean_dict:  # if variable in .csv-file (channelname) is in dictionary
        return clean_dict[channelname]  # return value (here: values of 'title', 'verticalaxislabel', 'normdatamin',
        # 'normdatamax',)


# DATA FROM .CSV-FILE: ------------------------------------------------------------------------------
# "A: leg (left/ right), B: specific variable, C: index of selected gait cycle starting from 0,
# D to end: normalized data (101 points from 0 to 100%)."


def get_prefix(file: Path) -> str:
    """
    function gets prefix of filepath
    param file: Path, e.g. ../test/P101/20210218/1013-Gait analysis - Forest road.csv
    return: string, prefix (=time of measurement), e.g. 1013
    """

    prefix = re.findall(r'(\d{4})-', str(file))[0]  # [0]: get first element in list
    return prefix


def add_side_to_var(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    function adds the letter 'l' (for left) or 'r' (for right) in front of each variable in a pandas dataframe. The
    side is at column 0, the variable at column 1.
    If variable does not begin with 'l' or 'r', an assertion is raised.
    param dataframe: pandas dataframe
    return: pandas dataframe
    """

    var_df = dataframe.iloc[:, 1]  # pull all values from var column in a list (e.g. 'Rotation TrunkRot')
    side_df = dataframe.iloc[:, 0]  # get corresponding side (e.g. 'right')
    side_df_first = side_df.str[0]  # get first letter of side (e.g. 'r')
    side_var = side_df_first + var_df  # concatenate (e.g. 'rRotation TrunkRot')

    for item in side_var.to_numpy():
        assert item[0].startswith('l') or item[0].startswith('r'), "variable does not begin with 'l' or 'r'"

    dataframe[1] = side_var  # replace var column with side_var
    return dataframe


def drop_exclude_graphs(dataframe: pd.Series) -> pd.Series:
    """
    Function to drop all values that are 'wrong', i.e. beginning with 'l' but being for the right side (e.g.
    'lPower RAnklePron').
    df.drop() works only on indexes, that's why the column with the variable-name in the series is looked for and then
     transformed into an index.
    axis=0 deletes the row.
    inplace=True does it on the given series (not on a copy).
    param dataframe: pandas series
    return: pandas series
    """

    for i in EXCLUDE_GRAPHS:
        dataframe.drop(dataframe.index[(dataframe.iloc[:] == i)], axis=0, inplace=True)
    return dataframe


def drop_unused_graphs(dataframe: pd.Series) -> pd.Series:
    """
    function to drop all values that are not used by the bemoved-team.
    df.drop() works only on indexes, that's why the column with the variable-name in the series is looked for and then
     transformed into an index.
    axis=0 deletes the row.
    inplace=True does it on the given series (not on a copy).
    param dataframe: pandas series
    return: pandas series
    """

    for i in GRAPHS_NOT_USED:
        dataframe.drop(dataframe.index[(dataframe.iloc[:] == i)], axis=0, inplace=True)
    return dataframe


def get_col_values(dataframe: pd.DataFrame, col: int) -> pd.Series:
    """
    Function returns a pandas' series of all values in a pandas dataframe that are at col index.
    Duplicates and wrong graphs are removed.
    param dataframe: pandas dataframe
    param col: integer, column index to retrieve values from
    return: pandas Series with all exclusive values in col
    """

    var = dataframe.iloc[:, col]  # pull all values from that column in a list
    var_excl = var.drop_duplicates()  # remove duplicates
    var_correct = drop_exclude_graphs(var_excl)  # remove wrong graphs
    var_unused = drop_unused_graphs(var_correct)  # remove unused graphs
    return var_unused


def get_graph_values(dataframe: pd.DataFrame, variable: str) -> pd.DataFrame:
    """
    Function returns all values for variable in dataframe. Each row contains 101 values for the matching
     variable/parameter.
    param dataframe: pandas dataframe
    param variable: string, variable for specific measurement/marker
    return: pandas dataframe with all values
    """

    variable_df = dataframe.loc[dataframe[1] == variable]  # get all rows that match variable (dataframe)
    graph_values = variable_df.iloc[:, 3:104]  # get only values for graph plotting / remove first three columns (
    # dataframe)
    return graph_values


def get_mean_values(dataframe: pd.DataFrame, variable: str) -> np.ndarray:
    """
    function returns a pandas' series with the mean value for each of the 101 measurements of variable.
    param dataframe: pandas dataframe
    param variable: string, variable for specific measurement/marker
    return: pandas series
    """

    df = get_graph_values(dataframe, variable)
    mean_column = np.mean(df)  # get mean value
    return mean_column


def get_std_deviation_value(dataframe: pd.DataFrame, variable: str) -> np.ndarray:
    """
    function returns a pandas' series with the standard deviation value for each of the 101 measurements of variable.
    param dataframe: pandas dataframe
    param variable: string, variable for specific measurement/marker
    :return: pandas series
    """

    df = get_graph_values(dataframe, variable)
    std_column = np.std(df)  # get standard deviation value
    return std_column


def df_to_list_of_lists(dataframe: Union[pd.Series, pd.DataFrame]) -> np.ndarray:
    """
    function turns a pandas dataframe or series to a list of lists
    param dataframe: pandas dataframe or series
    return: numpy ndarray as list of lists, values separated by space (not comma)
    """

    return dataframe.values


# PLOTTING THE GRAPHS: ------------------------------------------------------------------------------

# noinspection PyTypeChecker
def plot_graph(df: pd.DataFrame, directory: Path, prefix: str, variable: str, title: str, verticalaxislabel: str,
               normdatamin: list, normdatamax: list):
    """
    function produces graphs for df according to variable with matplotlib. Plotted are the mean graph and the
    standard deviation. Each graph with normdata is plotted twice: once with normdata (file ending: '-walk.svg'),
    once without (file ending: '-run.svg'). Graphs without normdata are plotted once (file ending: '-run-walk.svg').
    Graphs are saved in dir as .svg.
    param directory: string, directory of .csv-file
    param df: pandas dataframe
    param prefix: string, time of measurement
    param variable: string, variable for specific measurement/marker
    param title: string, optional parameter, German translation if given
    param verticalaxislabel: string, optional parameter if given
    param normdatamin: list, optional parameter, norm data min if given
    param normdatamax: list, optional parameter, norm data max if given
    return: no return value.
    """

    len_x = len(df_to_list_of_lists(get_graph_values(df, variable))[0])  # len of x (here: 101)
    x = list(range(len_x))  # list of values for x-axis (here: 0-101)
    plt.axhline(color=GRAIL_GRAPH_X_COLOR, linewidth=0.5)  # line at y=0
    plt.xticks(np.arange(0, len_x, 20), [f'{i}%' for i in np.arange(0, len_x, 20)])  # xticks increase by 20%
    plt.ylabel(verticalaxislabel)  # ylabel

    if title:
        side = 'rechts' if variable[:1] == 'r' else 'links'
        plt.title(f'{title} {side}', fontsize='large', fontweight='bold')
    else:
        side = 'right' if variable[:1] == 'r' else 'left'
        plt.title(f'{variable[1:]} {side}', fontsize='large', fontweight='bold')

    # PLOT MEAN GRAPH
    y_mean = df_to_list_of_lists(get_mean_values(df, variable))  # mean values
    mean_plot, = plt.plot(x, y_mean, color=GRAIL_GRAPH_MEAN_COLOR, zorder=10)

    # PLOT STANDARD DEVIATION
    y_std = df_to_list_of_lists(get_std_deviation_value(df, variable))  # std deviation
    graph_dev = plt.fill_between(x, y_mean - y_std, y_mean + y_std, color=GRAIL_GRAPH_STD_COLOR, zorder=5)

    graphs_path = directory / GRAPH_DIR / prefix
    Path.mkdir(graphs_path, parents=True, exist_ok=True)  # create dir for graphs

    # PLOT AND SAVE GRAPHS WITH NORMDATA:
    if normdatamax:
        # plot first without normdata ('-run'):
        plt.legend([(graph_dev, mean_plot)], [STANDARD_VALUE])  # plot legend
        svg_path = graphs_path / f'{prefix}-{variable}-run.{GRAPH_OUTPUT_FORMAT}'
        plt.savefig(svg_path, format=GRAPH_OUTPUT_FORMAT)  # 'save graph as .svg

        # plot then again with normdata ('-walk'):
        plt.plot(x, normdatamax, color=GRAIL_GRAPH_NORM_COLOR, zorder=0) # max norm line
        norm_graph = plt.fill_between(x, normdatamin, normdatamax, color=GRAIL_GRAPH_NORM_COLOR, zorder=0) # fill
        # norm between max and min norm line
        plt.plot(x, normdatamin, color=GRAIL_GRAPH_NORM_COLOR, zorder=0) # min norm line
        plt.legend([(graph_dev, mean_plot), norm_graph], [STANDARD_VALUE, NORM_VALUE]) # plot legend with normdata
        svg_path = graphs_path / f'{prefix}-{variable}-walk.{GRAPH_OUTPUT_FORMAT}'
        plt.savefig(svg_path, format=GRAPH_OUTPUT_FORMAT)  # 'save graph as .svg

    # PLOT AND SAVE GRAPHS WITHOUT NORMDATA ('-run-walk'):
    else:
        plt.legend([(graph_dev, mean_plot)], [STANDARD_VALUE])  # plot legend
        svg_path = graphs_path / f'{prefix}-{variable}-run-walk.{GRAPH_OUTPUT_FORMAT}'
        plt.savefig(svg_path, format=GRAPH_OUTPUT_FORMAT)  # 'save graph as .svg

    plt.close()  # close current graph

    write_readme(directory, prefix)


# WRITE README: ------------------------------------------------------------------------------

def write_readme(directory: Path, prefix: str):
    """
    function writes a readme-file in directory where the graphs are (in time/prefix directory).
    :return:
    """

    readme_path = directory / GRAPH_DIR / prefix / 'README.txt'
    readme_path.write_text(README_TEXT)


# MAIN FUNCTION ------------------------------------------------------------------------------

def generate_graphs(pat_id: str):
    """
    main function to loop through .csv-file and get all data to produce graphs.
    Get also all data from layout-file.
    param pat_id: string, patient id
    return: no return value
    """

    for file in get_csv_files(pat_id):
        prefix = get_prefix(file)
        directory = Path(file).parent  # get dir of file

        # LAYOUT FILES
        layout_walk = directory / NAME_LAYOUT_FILE_WALK

        # if os.path.isfile(LAYOUT_FILE_WALK):
        if LAYOUT_FILE_WALK.is_file():
            enhance_layout_file(LAYOUT_FILE_WALK, layout_walk)  # create new layout-file without markup
            # file 'walk.layout' is (over)written several times in case of several .csv-files - this is by design

            # create dataframe from layout-file:
            df_layout = create_dataframe(get_lookup_elements(get_root(layout_walk)))
            # pd.set_option("display.max_rows", 999)
            # pd.set_option("display.max_columns", 999)

            # JSON FILE
            # save German variable name, graph name and English variable name as .json-file:
            save_json(df_cols_to_json(df_layout), directory)

            # DATAFRAME (FROM .CSV-FILE)
            csv_df = pd.read_csv(file, header=None, low_memory=False)  # read .csv-file

            if csv_df.iloc[0, 0] != 'left':
                logging.warning('200. ERROR .csv-file')
                sg.popup('\nFalsche .csv-Datei.\nBitte "selected" statt "ALL" wählen.\n\n', title='Hinweis',
                         custom_text='Okay')

            else:
                df = add_side_to_var(csv_df)  # add 'r' or 'l' before variable name (e.g. 'lEMG LRectusFem_filtered')

                # FUNCTION CALL TO PLOT AND SAVE GRAPHS
                [plot_graph(df, directory, prefix, variable, **{key: get_layout_vars(df_layout, variable, key)
                                                                for key
                                                                in ('title', 'verticalaxislabel', 'normdatamin',
                                                                    'normdatamax')})
                 for variable in get_col_values(df, 1)]

        else:
            logging.warning('201. ERROR: layout file walk does not exist')
            sg.popup('\nDie .layout-Datei "walk" existiert nicht am angegebenen Pfad.\nDie Graphen können nicht '
                     'erzeugt werden.\n\n', title='Hinweis', custom_text='Okay')

        if LAYOUT_FILE_RUN.is_file():
            layout_run = directory / NAME_LAYOUT_FILE_RUN
            enhance_layout_file(LAYOUT_FILE_RUN, layout_run)  # copy run layout file (currently not used)
        else:
            logging.warning('202. ERROR: layout file run does not exist')
            sg.popup('\nDie .layout-Datei "run" existiert nicht am angegebenen Pfad.\nSie wird nicht auf den '
                     'Server kopiert.\n\n', title='Hinweis', custom_text='Okay')

        logging.debug('graphs were created')
        print('graphs were created')
