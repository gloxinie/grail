# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import os
from pathlib import Path

# LAYOUT
NAME_LAYOUT_FILE_WALK = 'walk.layout'
NAME_LAYOUT_FILE_RUN = 'run.layout'

# VM CASCADE
# SRC_PATH = Path('/home/dev/Git/grail/test/')
# SRC_VICON = Path('/home/dev/Git/grail/P')
# DEST_PATH = Path('/home/dev/shared_VM/test2/DLST_P')
# LAYOUT_FILE_WALK = Path('../JWI GANG Left -Right + GRF_german.layout')
# LAYOUT_FILE_RUN = Path('../MM Left -Right GRF run_german.layout')

# LOCAL:
# if 'TARGET' in os.environ and os.environ['TARGET'] == 'andy':
# SRC_PATH = Path('C:/Users/Andreas Hechler/Desktop/grail/test/')
# SRC_VICON = Path('C:/Users/Andreas Hechler/Desktop/grail/P')
# DEST_PATH = Path('C:/shared_VM/test2/DLST_P')
# LAYOUT_FILE_WALK = Path('C:/Users/Andreas Hechler/Desktop/grail/JWI GANG Left -Right + GRF_german.layout')
# LAYOUT_FILE_RUN = Path('C:/Users/Andreas Hechler/Desktop/grail/MM Left -Right GRF run_german.layout')

# CHARITÉ VICON (links/Server):
if 'TARGET' in os.environ and os.environ['TARGET'] == 'charite':
    print(os.environ['TARGET'])
SRC_PATH = Path('//192.168.1.50/Data_DFLOW/')  # \\192.168.1.50\Data_DFLOW\
SRC_VICON = Path('D:/BeMovedDB/gait analysis bemoved/P')
DEST_PATH = Path('S:/C09/JWI/MSB/DLST/Patienten/Daten/DLST_P')
LAYOUT_FILE_WALK = Path(f'{SRC_PATH}/layout/JWI GANG Left -Right + GRF_german.layout')
LAYOUT_FILE_RUN = Path(f'{SRC_PATH}/layout/MM Left -Right GRF run_german.layout')

# MARKUPS ETC. TO REPLACE
LIST_WITH_MARKUPS = ['<h2>', '</h2>', '<u>', '</u>', '<b>', '</b>', '<br>',
                     '<i>', '</i>']
LEFT_ARROW = '⬅ '
RIGHT_ARROW = ' ➡'

# COLUMN NAMES FOR DATAFRAME
DF_COLS = ['pagenumber', 'headertext', 'footertext', 'numberofcolumns',
           'numberofrows', 'page', 'row', 'column',
           'width', 'text', 'title', 'channelname', 'verticalaxislabel',
           'normdatamin', 'normdatamax',
           'verticalaxisminvalue', 'verticalaxismaxvalue']

# NAME OF JSON-FILE
NAME_OF_JSON_FILE = 'layout_file.json'


# GRAPHS ##################################################################################

# COLORS
GRAIL_GRAPH_X_COLOR = (0, 0, 0, 0.5)
GRAIL_GRAPH_STD_COLOR = 'lime'
GRAIL_GRAPH_NORM_COLOR = 'lightgray'
GRAIL_GRAPH_MEAN_COLOR = 'darkred'

# LABEL FOR GRAPHS
STANDARD_VALUE = 'Ihre Messwerte'
NORM_VALUE = 'Normbereich'

# DIR FOR GRAPHS
GRAPH_DIR = 'cascade_graphs'

# !! GRAPH OUTPUT FORMAT / DO NOT CHANGE !!
GRAPH_OUTPUT_FORMAT = 'svg'

# GRAPHS THAT SHOULD NOT BE CREATED:
# Explanation: Each file begins with either 'l' (for left) or 'r' (for right). Many files contain another capital 'L'
# or 'R'. If they match (e.g. 'lMoment LHipRot.svg') the graph is correct (2 l's). If they don't (e.g. 'lMoment
# RHipRot.svg') the graph is not correct (1 l, 1 r). Those graphs are created but not used. In a similar way this is
# true for filenames containing 'FP0' (= left) and 'FP1' (= right). (Value of the tag 'leftside': '0' is for right
# cycles and '1' for left cycles.)
# Exceptions are 'PelvicObl', 'PelvicRot' and 'PelvicTil' and all the 'Trunk' values since human beings have only one
# trunk and pelvis and not two. Here, left AND right are correct and can be used (e.g. 'rRotation TrunkTilt.svg' and
# 'lRotation TrunkTilt.svg').
EXCLUDE_GRAPHS = ['lEMG RBicepsFem_filtered', 'lEMG RGastrocMed_filtered', 'lEMG RSemitendi_filtered',
                  'lEMG RSoleus_filtered', 'lMoment RAnkleAbAd', 'lMoment RAnkleFlex',
                  'lMoment RAnklePron', 'lMoment RAnkleRot', 'lMoment RHipAbAd', 'lMoment RHipFlex', 'lMoment RHipRot',
                  'lMoment RKneeAbAd', 'lMoment RKneeFlex', 'lMoment RKneeRot', 'lPower RAnkleAbAd',
                  'lPower RAnkleFlex', 'lPower RAnklePron', 'lPower RAnkleRot', 'lPower RHipAbAd', 'lPower RHipFlex',
                  'lPower RHipRot', 'lPower RKneeAbAd', 'lPower RKneeFlex', 'lPower RKneeRot', 'lR.Cadence',
                  'lR.Initial.Double.Support', 'lR.Single.Support', 'lR.Stance.Swing', 'lR.Stance.Time',
                  'lR.Step.Length', 'lR.Step.Time', 'lR.Step.Width', 'lR.Stride.Length', 'lR.Stride.Time',
                  'lR.Swing.Time', 'lR.Terminal.Double.Support', 'lR.Total.Double.Support', 'lRProgression',
                  'lRotation RAnkleAbAd', 'lRotation RAnkleFlex', 'lRotation RAnklePron', 'lRotation RAnkleRot',
                  'lRotation RHipAbAd', 'lRotation RHipFlex', 'lRotation RHipRot', 'lRotation RKneeAbAd',
                  'lRotation RKneeFlex', 'lRotation RKneeRot', 'lFP1 - COP X', 'lFP1_filtered - COP X', 'lFP1 - COP Y',
                  'lFP1_filtered - COP Y', 'lFP1 - COP Z', 'lFP1_filtered - COP Z', 'lFP1 - Force X',
                  'lFP1_filtered - Force X', 'lFP1 - Force Y', 'lFP1_filtered - Force Y', 'lFP1 - Force Z',
                  'lFP1_filtered - Force Z', 'lFP1 - Moment X', 'lFP1_filtered - Moment X', 'lFP1 - Moment Y',
                  'lFP1_filtered - Moment Y', 'lFP1 - Moment Z', 'lFP1_filtered - Moment Z',
                  'rEMG LGastrocMed_filtered', 'rEMG LRectusFem_filtered', 'rEMG LSoleus_filtered',
                  'rEMG LVastusLat_filtered', 'rEMG LVastusMed_filtered', 'rL.Cadence', 'rL.Initial.Double.Support',
                  'rL.Single.Support', 'rL.Stance.Swing', 'rL.Stance.Time', 'rL.Step.Length', 'rL.Step.Time',
                  'rL.Step.Width', 'rL.Stride.Length', 'rL.Stride.Time', 'rL.Swing.Time',
                  'rL.Terminal.Double.Support', 'rL.Total.Double.Support', 'rLProgression', 'rMoment LAnkleAbAd',
                  'rMoment LAnkleFlex', 'rMoment LAnklePron', 'rMoment LAnkleRot', 'rMoment LHipAbAd',
                  'rMoment LHipFlex', 'rMoment LHipRot', 'rMoment LKneeAbAd', 'rMoment LKneeFlex',
                  'rMoment LKneeRot', 'rPower LAnkleAbAd', 'rPower LAnkleFlex', 'rPower LAnklePron',
                  'rPower LAnkleRot', 'rPower LHipAbAd', 'rPower LHipFlex', 'rPower LHipRot', 'rPower LKneeAbAd',
                  'rPower LKneeFlex', 'rPower LKneeRot', 'rRotation LAnkleAbAd', 'rRotation LAnkleFlex',
                  'rRotation LAnklePron', 'rRotation LAnkleRot', 'rRotation LHipAbAd', 'rRotation LHipFlex',
                  'rRotation LHipRot', 'rRotation LKneeAbAd', 'rRotation LKneeFlex', 'rRotation LKneeRot',
                  'rFP0 - COP X', 'rFP0_filtered - COP X', 'rFP0 - COP Y', 'rFP0_filtered - COP Y', 'rFP0 - COP Z',
                  'rFP0_filtered - COP Z', 'rFP0 - Force X', 'rFP0_filtered - Force X', 'rFP0 - Force Y',
                  'rFP0_filtered - Force Y', 'rFP0 - Force Z', 'rFP0_filtered - Force Z', 'rFP0 - Moment X',
                  'rFP0_filtered - Moment X', 'rFP0 - Moment Y', 'rFP0_filtered - Moment Y', 'rFP0 - Moment Z',
                  'rFP0_filtered - Moment Z']

# GRAPHS NOT USED BY BEMOVED-TEAM:
GRAPHS_NOT_USED = ['lEMG LGastrocMed_filtered', 'lEMG LRectusFem_filtered', 'lEMG LSoleus_filtered',
                   'lEMG LVastusLat_filtered', 'lEMG LVastusMed_filtered', 'lFP0 - COP X', 'lFP0_filtered - COP X',
                   'lFP0 - COP Y', 'lFP0_filtered - COP Y', 'lFP0 - COP Z', 'lFP0_filtered - COP Z',
                   'lFP0_filtered - Force X', 'lFP0_filtered - Force Y', 'lFP0_filtered - Force Z', 'lFP0 - Moment X',
                   'lFP0_filtered - Moment X', 'lFP0 - Moment Y', 'lFP0_filtered - Moment Y', 'lFP0 - Moment Z',
                   'lFP0_filtered - Moment Z', 'lL.Cadence', 'lL.Initial.Double.Support', 'lL.Single.Support',
                   'lL.Stance.Swing', 'lL.Stance.Time', 'lL.Step.Length', 'lL.Step.Time', 'lL.Step.Width',
                   'lL.Stride.Length', 'lL.Stride.Time', 'lL.Swing.Time', 'lL.Terminal.Double.Support',
                   'lL.Total.Double.Support', 'lMoment LAnkleAbAd', 'lMoment LAnklePron', 'lMoment LAnkleRot',
                   'lMoment LKneeRot', 'lMoment PelvicObl', 'lMoment PelvicRot', 'lMoment PelvicTil',
                   'lMoment TrunkFlex', 'lMoment TrunkRot', 'lMoment TrunkTilt', 'lPower LAnkleAbAd',
                   'lPower LAnklePron', 'lPower LAnkleRot', 'lPower LHipAbAd', 'lPower LHipRot', 'lPower LKneeAbAd',
                   'lPower LKneeRot', 'lPower PelvicObl', 'lPower PelvicRot', 'lPower PelvicTil', 'lPower TrunkFlex',
                   'lPower TrunkRot', 'lPower TrunkTilt', 'lRotation LAnkleAbAd', 'lRotation LAnkleRot',
                   'lRotation LKneeAbAd', 'lRotation LKneeRot', 'lRotation TrunkFlex_Global',
                   'lRotation TrunkRot_Global', 'lRotation TrunkTilt_Global', 'lWalking.Speed',
                   'rEMG RBicepsFem_filtered', 'rEMG RGastrocMed_filtered', 'rEMG RSemitendi_filtered',
                   'rEMG RSoleus_filtered', 'rFP1 - COP X', 'rFP1_filtered - COP X', 'rFP1 - COP Y',
                   'rFP1_filtered - COP Y', 'rFP1 - COP Z', 'rFP1_filtered - COP Z', 'rFP1_filtered - Force X',
                   'rFP1_filtered - Force Y', 'rFP1_filtered - Force Z', 'rFP1 - Moment X', 'rFP1_filtered - Moment X',
                   'rFP1 - Moment Y', 'rFP1_filtered - Moment Y', 'rFP1 - Moment Z', 'rFP1_filtered - Moment Z',
                   'rMoment PelvicObl', 'rMoment PelvicRot', 'rMoment PelvicTil', 'rMoment RAnkleAbAd',
                   'rMoment RAnklePron', 'rMoment RAnkleRot', 'rMoment RKneeRot', 'rMoment TrunkFlex',
                   'rMoment TrunkRot', 'rMoment TrunkTilt', 'rPower PelvicObl', 'rPower PelvicRot', 'rPower PelvicTil',
                   'rPower RAnkleAbAd', 'rPower RAnklePron', 'rPower RAnkleRot', 'rPower RHipAbAd', 'rPower RHipRot',
                   'rPower RKneeAbAd', 'rPower RKneeRot', 'rPower TrunkFlex', 'rPower TrunkRot', 'rPower TrunkTilt',
                   'rR.Cadence', 'rR.Initial.Double.Support', 'rR.Single.Support', 'rR.Stance.Swing',
                   'rR.Stance.Time', 'rR.Step.Length', 'rR.Step.Time', 'rR.Step.Width', 'rR.Stride.Length',
                   'rR.Stride.Time', 'rR.Swing.Time', 'rR.Terminal.Double.Support', 'rR.Total.Double.Support',
                   'rRotation RAnkleAbAd', 'rRotation RAnkleRot', 'rRotation RKneeAbAd', 'rRotation RKneeRot',
                   'rRotation TrunkFlex_Global', 'rRotation TrunkRot_Global', 'rRotation TrunkTilt_Global',
                   'rWalking.Speed']

# DIRECTORIES ON SERVER (OTHER THAN GRAIL)
OTHER_DIRS = ['Dokumente', 'Vicon']

# TEXT FOR README
README_TEXT = '''IMPORTANT INFORMATION

Graphs were generated by Cascade Informationssysteme GmbH (cascade.de).

Not all graphs created are needed and/or used.
This is by design.
'''
