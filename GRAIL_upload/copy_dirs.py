# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import logging
import os.path
import platform
import re
import shutil
from datetime import datetime
from pathlib import Path
from typing import List, Union

import PySimpleGUI as sg

from GRAIL_upload.config import DEST_PATH, OTHER_DIRS, SRC_VICON
from GRAIL_upload.utils import extract_pat_id, local_pat_path

# get date of today
current_date = datetime.today().strftime('%Y%m%d')
current_date_old_structure = datetime.today().strftime('%y%m%d')


def remote_pat_path(pat_id: str) -> str:
    """
    return the absolute path to the remote patients' directory
    :param pat_id: string, patient's id
    :return: string, absolute path to the remote patient's directory
    """

    result = f'{DEST_PATH}{pat_id}'
    assert extract_pat_id(result) == pat_id, 'assert in remote_pat_path(pat_id) failed'
    return result


def extract_pat_date(path: str) -> Union[str, None]:
    """
    Return pat_date from given path.
    Error message if path is empty string or no valid date structure can be found.
    Throws exception if parameter is empty.
    param path: string, dir path
    return: string, 8-digit date
    """

    assert path != '', 'path is empty'

    # regex pattern to find date-like-structure in path, 8 (20210823) consecutive digits:
    date = re.findall(r'\d{8}', path)

    if date:
        return date[0]

    logging.warning(f'05. date not found in path: {path}')
    sg.popup(
        f'\nDer Datumsordner dieses Pfads\n{path}\nfehlt oder ist '
        f'fehlerhaft und kann nicht kopiert werden.\n'
        f'Bitte einen Datumsordner im Format yyyymmdd anlegen, die zu '
        f'kopierenden Dateien dort hineinlegen und'
        f' erneut versuchen.\n\n', title='Hinweis', custom_text='Okay')
    return None


def find_dates_on_local(pat_id: str) -> List[str]:
    """
    return list of strings with dates of dirs in remote patient dir
    returns empty list if pat_id does not exist
    param pat_id: string, patient's id
    return: list with strings containing dates in local patient's dir
    """

    # get dates from dir-paths one level down (= dates) and put into list:
    list_pat_dates = [extract_pat_date(str(path)) for path in local_pat_path(pat_id).glob('*')]

    # filter out None:
    list_pat_dates_not_empty = [x for x in list_pat_dates if x]
    return list_pat_dates_not_empty


def from_eight_to_six(date: str) -> str:
    """
    Change 8-digit date (yyyymmdd) to 6-digit date (yymmdd).
    param date: string, 8-digit date
    return: string, 6-digit-date
    """

    return date[2:]  # skip first two digits of year (20210305 --> 210305)


def fetch_vicon_dirs(pat_id: str) -> List[Path]:
    """
    Return list of strings with directories in local patient dir on Vicon computer.
    param pat_id: string, patient's id
    return: list of strings containing directory-names always beginning with "New Session", e.g. ['New Session',
    'New Session1', New Session2']
    """

    path = Path(f'{SRC_VICON}{pat_id}')

    if path.exists():
        list_vicon_dirs = [i for i in path.iterdir() if i.is_dir()]
        return list_vicon_dirs


def get_file_creation_date(file: Path) -> str:
    """
    Function gets creation date of the given file.
    Differs for varying operating systems, therefore first check for operating system.
    For Windows sometimes modified date is more accurate than creation date - the smaller one is taken.
    param file: Path, path of file in Vicon directory
    return: string, date as yymmdd
    """
    if platform.system() == 'Windows':
        path_creation_time = os.path.getctime(file)  # get creation date (epoch) for each file in windows
        path_modified_time = os.path.getmtime(file)  # get modified date (epoch) for each file in windows
        # take the smaller value - is closer to the actual creation date:
        file_epoch_creation = path_creation_time if path_creation_time < path_modified_time else path_modified_time
    else:  # mac os and linux
        stat = os.stat(file)
        try:
            file_epoch_creation = stat.st_birthtime  # for mac
        except AttributeError:  # Linux has no creation dates --> check for last modified
            file_epoch_creation = stat.st_mtime

    # change from epoch time to YYMMDD:
    file_creation = datetime.fromtimestamp(file_epoch_creation).strftime('%y%m%d')
    return file_creation


def copy_local_to_remote_path(pat_id: str):
    """
    Copy all local data of patient to server.
    Throw exception in case of any error.
    param pat_id: string, patient's id
    """

    for date in find_dates_on_local(pat_id):
        date_six = from_eight_to_six(date)
        path_src = Path(local_pat_path(pat_id), date)
        path_dest = Path(remote_pat_path(pat_id), date_six)

        # copies local GRAIL pat-dir recursively to server GRAIL pat-dir, even if patient_dir already exists
        shutil.copytree(path_src, path_dest / 'GRAIL', dirs_exist_ok=True)

        # create other directories
        try:
            for other_dir in OTHER_DIRS:
                Path.mkdir(path_dest / other_dir)
        except FileExistsError as e:
            logging.warning(f'83. directory exists already, {e}')
        except FileNotFoundError as e:
            logging.warning(f'84. missing parent(s) of path, {e}')

        # copies local Vicon-dirs to server Vicon-dir, even if dir already exists
        vicon_dirs = fetch_vicon_dirs(pat_id)  # get list of vicon dirs
        if vicon_dirs is not None:
            for src_path in vicon_dirs:  # iterate through vicon dirs
                for file in src_path.iterdir():  # get path for each file in each dir
                    file_creation = get_file_creation_date(file)
                    if file_creation == date_six:
                        # if file creation date (e.g. '210624') is equal to dir-name (e.g. '210624'), copy that file
                        # into Vicon-dir (e.g. '/210624/Vicon/')
                        shutil.copy2(file, path_dest / 'Vicon')
