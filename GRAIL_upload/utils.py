# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import logging
import re
from pathlib import Path
from typing import List

from GRAIL_upload.config import SRC_PATH


def extract_pat_id(path: str) -> str:
    """
    Return pat_id from given local path.
    The path can point to any directory of a local patient's path.
    Return empty string if path does not contain a pat_id or if pat_id is out of range.
    Throws exception if the path is not a string.
    param path: string, path of patient
    return: string, pat_id (patient id)
    """

    assert path != '', 'path is empty'

    pat_id_in_list = re.findall(r'P(\d{3,4})', path)  # regex pattern to find 3 or 4 digits after 'P'
    if not pat_id_in_list:
        logging.warning(f'05. pat_id not found in path: {path}')
        return ''
    pat_id = pat_id_in_list[0]

    # user input has to be 3 or 4 digits, otherwise set empty string
    if not len(pat_id) in (3, 4):
        logging.warning(f'04. pat_id is out of range, pat_id: {pat_id}, path: {path}')
        pat_id = ''

    return pat_id


def fetch_sort_all_pat_ids() -> List[str]:
    """
    Fetch all pat_ids on local GRAIL and sort from highest to lowest
    return: list with local pat_ids as strings sorted from highest to lowest
    """

    # get pat_ids from local dir-paths and append to list:
    list_pat_ids = [extract_pat_id(str(path)) for path in SRC_PATH.glob('*')]  # e.g. ['100', '', '101', '102']

    # remove empty strings
    list_pat_ids_not_empty = [x for x in list_pat_ids if x]  # e.g. ['100', '101', '102']

    # sort list from highest to lowest number (only possible for int, not for str):
    sorted_list_pat_ids = sorted(list_pat_ids_not_empty, key=lambda num: int(num), reverse=True)
    return sorted_list_pat_ids


def local_pat_path(pat_id: str) -> Path:
    """
    return the absolute path to the local patients' directory
    param pat_id: string, patient id
    return: Path, absolute path to local patient dir
    """

    result = Path(SRC_PATH,  'P' + pat_id)
    assert extract_pat_id(str(result)) == pat_id, 'assert in local_pat_path(pat_id) failed'
    return result


def get_csv_files(pat_id: str) -> List[Path]:
    """
    function checks if there are .csv-files in local directory.
    param pat_id: string, patient's id
    return files: list of Paths of .csv-files
    """

    local_path = local_pat_path(pat_id)
    files = local_path.glob('*/*.csv')  # generator object (list with Paths)
    return list(files)
