# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import logging
import shutil
import sys
from datetime import datetime
from pathlib import Path

import isort
import PySimpleGUI as sg

from GRAIL_upload.config import DEST_PATH, SRC_PATH
from GRAIL_upload.copy_dirs import copy_local_to_remote_path
from GRAIL_upload.generating_graphs import generate_graphs
from GRAIL_upload.utils import fetch_sort_all_pat_ids, get_csv_files

# sort import with isort
sort_files = Path('.').glob('**/*.py')
[isort.file(sort_file) for sort_file in sort_files]


# create log-file and log warnings and other debugs
logging.basicConfig(filename=f'{SRC_PATH}GRAIL_upload.log', level=logging.DEBUG,
                    format='%(message)s  |  %(levelname)s %(asctime)s')
logging.getLogger('matplotlib.font_manager').disabled = True  # do not log missing fonts
# if next line uncommented: debug messages do not appear
# logging.disable(logging.CRITICAL)


def copy_local_to_remote_slot(pat_id: str):
    """
    Copy all local data for the given pat_id to the remote location on the server.
    Throw exception in case of any error.
    param pat_id: string, patient's id
    """

    generate_graphs(pat_id)
    copy_local_to_remote_path(pat_id)


def gui_layout() -> list:
    """
    # Create layout (positioned elements) for PySimpleGUI.
    :return: list/object
    """

    col_pat_id = [
        [sg.Text(text='Patient:innennummer:', auto_size_text=True),
         sg.Input(size=(4, 1), justification='c', background_color='white',
                  text_color='black',
                  enable_events=True, key='-ID-', focus=True)
         ]
    ]

    col_pat_info = [
        [sg.Text(text='Patient:in gefunden', enable_events=True,
                 text_color='red', key='-NOTFOUND-', visible=False),
         sg.Text(text='keine .csv-Datei    ', enable_events=True,
                 text_color='red', key='-NOCSV-', visible=False)]
    ]

    col_copy = [[sg.Text(auto_size_text=True, size=(30, 3), pad=(0, 0),
                         enable_events=True, key='-COPYFEEDBACK-',
                         visible=False)]]

    col_fertig = [[sg.Button('Fertig', disabled=False, key='-FERTIG-')]]

    layout = [
        [sg.Text('GRAIL upload kopiert die lokalen Daten auf den Server.\n',
                 auto_size_text=True,
                 pad=((20, 60), (20, 0)))],
        [sg.Text(text=f'Suchpfad:  {SRC_PATH}', pad=((20, 0), 0)),
         sg.Text(size=(5, 1), pad=(0, 0), enable_events=True, key='-SRCPATH-',
                 visible=False)],
        [sg.Text(f'Zielpfad:   ', pad=((20, 0), 0)),
         sg.Text(size=(45, 1), pad=(0, 0), enable_events=True, key='-DESTPATH-',
                 visible=False)],

        [sg.Column(col_pat_id, pad=((20, 5), (50, 10)), justification='left',
                   element_justification='left',
                   vertical_alignment='top'),
         sg.Column(col_pat_info, pad=((0, 20), (50, 10)), justification='left',
                   element_justification='left',
                   vertical_alignment='top')],

        [sg.Button(f'Daten auf Server kopieren', pad=(20, 20), disabled=True,
                   key='-COPYPAT-')],

        [sg.Column(col_copy, pad=((20, 0), 0), justification='left',
                   element_justification='left',
                   vertical_alignment='top'),
         sg.Column(col_fertig, pad=((0, 10), (60, 10)), justification='right',
                   element_justification='right',
                   vertical_alignment='bottom')]
    ]
    return layout


def user_feedback(window: sg.PySimpleGUI.Window, value_found='', txt_col='red', id_feedback=False, value_csv='',
                  no_csv_file=False, disable_copy_button=True, pat_id=None, source_path=False,
                  destination_path=False, value_copy='', copy_feedback=False, disable_fertig_button=False):
    """
    Function to return user feedback as text and to en-/disable copy button depending on user input.
    :param window: class 'PySimpleGUI.PySimpleGUI.Window', window object
    :param value_found: optional string, defaults to empty string, text shown in GUI
    :param txt_col: optional string, defaults to 'white', color of text
    :param id_feedback: optional bool, defaults to False, show text if pat_id not found
    :param value_csv: optional string, defaults to empty string, text shown in GUI
    :param no_csv_file: optional bool, defaults to False, show text if no .csv-file is found
    :param disable_copy_button: optional bool, defaults to True, enable button if False
    :param pat_id: optional string, defaults to None, user input patient ID
    :param source_path: optional bool, defaults to False, show pat_id on user input
    :param destination_path: optional bool, defaults to False, show destination path if copying is possible
    :param value_copy: optional string, defaults to empty string, text shown in GUI
    :param copy_feedback: optional bool, defaults to False, show text if feedback for use
    :param disable_fertig_button: optional bool, defaults to False, disable button if True
    """

    window['-SRCPATH-'].update(value=f'P{pat_id}', visible=source_path)
    window['-DESTPATH-'].update(value=f'{DEST_PATH}{pat_id}',
                                visible=destination_path)
    window['-NOTFOUND-'].update(value=value_found, text_color=txt_col,
                                visible=id_feedback)
    window['-NOCSV-'].update(value=value_csv, visible=no_csv_file)
    window['-COPYPAT-'].update(disabled=disable_copy_button)
    window['-COPYFEEDBACK-'].update(value=value_copy, text_color=txt_col,
                                    visible=copy_feedback)
    window['-FERTIG-'].update(disabled=disable_fertig_button)
    window.refresh()


def check_user_input(window: sg.PySimpleGUI.Window, values: dict):
    """
    Function to check user input. User input is number as string. Number has to have 3 or 4 digits - otherwise
    functions are called to tell user to input correct length.
    If number exists as patient id, green button is shown and copy-button is enabled.
    If number does not exist as patient id, user learns that patient could not be found.
    param window: class 'PySimpleGUI.PySimpleGUI.Window', window object
    param values: dictionary, key-value-pair, e.g. {'-ID-': '1'}
    """

    pat_id = int(values['-ID-'], base=10)  # convert user input to int
    if len(str(pat_id)) < 3:
        user_feedback(window)
    elif len(str(pat_id)) > 4:
        user_feedback(window, value_found='Nummer zu lang', id_feedback=True)
    else:
        # check if pat_id exists: loop through all pat_ids and check them
        # against user input
        if str(pat_id) in fetch_sort_all_pat_ids():
            # if yes: show 'Patient gefunden' and enable button for copy
            if not get_csv_files(str(pat_id)):  # does .csv-file exist?
                user_feedback(window, value_found='Patient:in gefunden',
                              txt_col='green', id_feedback=True,
                              value_csv='keine .csv-Datei    ',
                              no_csv_file=True, disable_copy_button=False,
                              pat_id=str(pat_id), source_path=True,
                              destination_path=True)
            else:
                user_feedback(window, value_found='Patient:in gefunden',
                              txt_col='green', id_feedback=True,
                              disable_copy_button=False, pat_id=str(pat_id),
                              source_path=True, destination_path=True)
        else:
            # if no: show 'Nicht gefunden'
            if len(str(pat_id)) > 2:  # show only when min. 3 digits
                user_feedback(window, value_found='Nicht gefunden',
                              id_feedback=True)


def gui():
    """ Function to initiate PySimpleGUI event loop. """

    # sg.theme('Black')
    sg.theme('DarkGrey14')

    window = sg.Window(title='GRAIL upload', layout=gui_layout(),
                       icon='icon31.ico')  # PROGRAMMNAME: 'GRAIL upload'
    # icon works only in Windows

    while True:
        event, values = window.read()

        if event in (None, sg.WIN_CLOSED):
            break
        if event == '-FERTIG-':
            sys.exit(0)

        pat_id = values['-ID-']
        if pat_id:  # user input
            try:
                check_user_input(window, values)
            except ValueError:
                user_feedback(window, value_found='Nur Ziffern erlaubt.',
                              id_feedback=True)  # user input is not int

        # copy data from local pat_id to server
        if event == '-COPYPAT-':
            copy_begin = datetime.today()

            # message: is copying
            user_feedback(window, txt_col='white', pat_id=pat_id,
                          source_path=True, destination_path=True,
                          value_copy='... am Kopieren ...', copy_feedback=True,
                          disable_fertig_button=True)

            try:
                copy_local_to_remote_slot(pat_id)  # copy function
            except shutil.Error:
                # error-message
                user_feedback(window, disable_copy_button=False, pat_id=pat_id,
                              source_path=True, destination_path=True,
                              value_copy='Kopiervorgang fehlgeschlagen.\nBitte Service (Cascade) kontaktieren.',
                              copy_feedback=True)

            # message: copying done
            user_feedback(window, txt_col='green', disable_copy_button=False,
                          pat_id=pat_id, source_path=True,
                          destination_path=True, copy_feedback=True,
                          value_copy='Kopiervorgang fertig')

            copy_end = datetime.today()
            print(f'copying lasted: {copy_end - copy_begin}')

    window.close()


if __name__ == '__main__':
    logging.debug('\n\n\n\n*************************** NEXT PATIENT ***************************')
    gui()
