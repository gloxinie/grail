# Copyright (c) 2021 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import json
import re
import shutil
import xml.etree.ElementTree as et
from pathlib import Path
from typing import List

import pandas as pd

from GRAIL_upload.config import DF_COLS, LEFT_ARROW, LIST_WITH_MARKUPS, NAME_OF_JSON_FILE, RIGHT_ARROW


def enhance_layout_file(input_file: Path, output_file: Path):
    """
    Original file contains HTML code, in particular '<' and '>', which cannot be parsed by etree.
    Copy layout-file and remove all markups and make nicer arrows.
    param input_file: string, .layout-file, layout file walk
    param output_file: string .layout-file, layout file walk
    """

    shutil.copyfile(input_file, output_file)  # copy and rename layout-file

    with output_file.open(encoding='utf-8') as f:
        text = f.read()

        for marker in LIST_WITH_MARKUPS:
            text = text.replace(marker, '')
        text = text.replace(r'ï»¿', '')  # potential error when file is copied
        text = re.sub(r'<-', LEFT_ARROW, text)  # nicer left arrow
        text = re.sub(r'->', RIGHT_ARROW, text)  # nicer right arrow
        text = re.sub('- ➡', '-->', text)  # comments in file should stay intact (are destroyed through regex in line
        # above)

    output_file.write_text(text, encoding='utf-8')


def get_root(layout_file: str) -> et.Element:
    """
    Function gets the tree structure of layout_file.
    param layout_file: .layout-file, layout file walk
    return:
    """

    tree = et.parse(layout_file)  # layout-file walk
    root = tree.getroot()
    return root


def get_channelname_values(element: et.Element, lookup_elements: et.Element, rows: List[dict], index: int):
    """
    Function looks for corresponding values if key is 'channelname' in layout-file and prefixes 'r' (for 'right') or
    'l' (for 'left')
    param element: xml.etree.ElementTree.Element
    param lookup_elements: xml.etree.ElementTree.Element
    param rows: list, list of dictionaries
    param index: integer
    return: no return value
    """

    value_of_var = element.get('value')
    for bool_element in lookup_elements.findall('Boolean'):
        if bool_element.get('tag') == 'leftside':
            leftside = bool_element.get('value')
            leftside_one_letter = 'r' if leftside == '0' else 'l'
            value_of_var = leftside_one_letter + value_of_var  # add 'l' or 'r' before value of channelname
    rows[index].update({'channelname': value_of_var})


def get_values(element: et.Element, j: str, rows: List[dict], index: int):
    """
    Function looks for corresponding values for specific keys in layout-file
    param element: xml.etree.ElementTree.Element
    param j: string, value in list all_variables
    param rows: list of dictionaries
    param index: integer
    """

    if element.get('tag') == j:
        value_of_var = element.get('value')
        rows[index].update({j: value_of_var})


def get_norm_values(element: et.Element, k: str, rows: List[dict], index: int):
    """
    Function looks for corresponding values for normdata min and max in layout-file
    :param element: class 'xml.etree.ElementTree.Element
    :param k: string, value in list normdata_variables
    :param rows: list, list of dictionaries
    :param index: integer
    """

    if element.get('tag') == k:
        normdata = element.get('value')
        normdata_comma_list = normdata.split()  # return comma-separated list
        normdata_float = [float(i) for i in normdata_comma_list]  # turn str to float
        rows[index].update({k: normdata_float})  # map list to column k


def get_elements(element: et.Element, lookup_elements: et.Element, rows: List[dict], index: int):
    """
    Function looks for specific keys in layout-file and calls functions to get corresponding values.
    param element: xml.etree.ElementTree.Element
    param lookup_elements: xml.etree.ElementTree.Element
    param rows: list of dictionaries
    param index: integer
    """

    all_variables = ['headertext', 'footertext', 'text', 'title', 'verticalaxislabel', 'pagenumber', 'numberofcolumns',
                     'numberofrows', 'page', 'row', 'column', 'width', 'verticalaxisminvalue', 'verticalaxismaxvalue']
    normdata_variables = ['normdatamin', 'normdatamax']

    if len(rows) <= index:
        rows.append({})

    if element.get('tag') == 'channelname':
        get_channelname_values(element, lookup_elements, rows, index)

    for j in all_variables:
        get_values(element, j, rows, index)

    for k in normdata_variables:
        get_norm_values(element, k, rows, index)


def get_lookup_elements(root: et.Element) -> List[dict]:
    """
    Function looks for specific elements in layout-file.
    If found, it calls a function to get specific keys.
    param root: etree-element from xml-file, xml.etree.ElementTree.Element
    return: list of dictionaries with key-value-pairs
    """

    rows = []
    datatype_variables = ['String', 'Integer', 'Real']

    for index, lookup_elements in enumerate(root.findall('Group/Lookup')):
        for i in datatype_variables:
            for element in lookup_elements.findall(i):
                get_elements(element, lookup_elements, rows, index)
    return rows


def create_dataframe(rows_input: List[dict]) -> pd.DataFrame:
    """
    Function returns a pandas dataframe out of a list as parameter.
    Column names are hardcoded in config.py.
    param rows_input: list
    return: pandas dataframe
    """

    rows_input = list(filter(None, rows_input))  # filter empty dicts

    df = pd.DataFrame(rows_input, columns=DF_COLS)
    # df = df.where(pd.notnull(df), None)
    # pd.set_option("display.max_rows", 999)
    # pd.set_option("display.max_columns", 999)
    # print("DF:", df)
    return df


def df_cols_to_json(dataframe: pd.DataFrame) -> str:
    """
    Function to return 3 pandas DataFrame-columns in JSON-formatted string.
    JSON contains 3 keys: title (German name), graphname, channelname (English name)
    param dataframe: pandas DataFrame
    return: string containing list with dictionaries
    """

    title = dataframe.loc[:, 'title'].dropna()  # get all title values, drop NaN
    graphname = dataframe.loc[:, 'channelname'].dropna().rename('graphname')  # get all channelname-values with 'l'
    # or 'r' as first letter, drop NaN, rename column to 'graphname'
    channelname = dataframe.loc[:, 'channelname'].str[1:].dropna()  # get all channelname values, delete first letter
    # ('l' or 'r') of channelname, drop NaN
    df = pd.concat([title, graphname, channelname], axis=1)  # create new dataframe of title, graphname and channelname

    result = df.to_json(orient='records', force_ascii=False)  # to json: list with dics
    parsed = json.loads(result)  # parse: correct spacing
    dumps = 'Lookup:\n' + json.dumps(parsed, indent=4, ensure_ascii=False)  # format json
    return dumps


def save_json(file: str, directory: Path):
    """
    Function to write JSON-formatted string to JSON-file.
    param file: string
    param: directory, absolute path of .csv-file
    """

    json_path = directory / NAME_OF_JSON_FILE
    json_path.write_text(file)
