# About GRAIL_upload

[toc]



## General info

**GRAIL_upload** copies the local data from GRAIL and Vicon to the BeMoveD-server of Charité Berlin.
The date is converted from 8 digits (`yyyymmdd`) to 6 digits (`yymmdd`) and a specific folder structure is created.
Graphs in `.svg` format are also created, if a `.csv`-file is available.
A `.json`-file is also created for later use and the two `.layout`-files are also copied.

<img src="images/grail_upload.gif" alt="gif to display some images from GRAIL_upload GUI">


## Important
The two `.layout-files` (`JWI GANG Left -Right + GRF_german.layout`and `MM Left -Right GRF run_german.layout`) have
to be in a `layout`-directory. This directory is in the following path: `\\192.168.1.50\Data_DFLOW\`.



## Technologies

The code is written in Python 3.8.10

Additional libraries installed and used are Pandas (1.3.3), Matplotlib (3.4.3) , PySimpleGui (4.47.0).



## Create .exe-file for Windows OS

`cd` into dir `GRAIL_upload`. Use PyInstaller to create one single file:

```
pyinstaller -F -w --icon=icon31.ico --name 'GRAIL_upload' gui.py
```

With target:

```
TARGET=charite pyinstaller -F -w --icon=../icon31.ico --name 'GRAIL_upload' gui.py
```

If you want to create several files:

```
pyinstaller -w --icon=../icon31.ico --name 'GRAIL_upload' gui.py
```

If you are on a Windows computer you get a file called `GRAIL_upload.exe`.



## License
[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)